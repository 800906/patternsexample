﻿using System;
using System.IO;

namespace PatternsExample
{
    interface IFileVisitor
    {
        string DestinationFolder { get; }
        bool IsMatch(FileInfo file);
    }
}
