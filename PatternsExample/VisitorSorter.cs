﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PatternsExample
{
    public class VisitorSorter
    {
        class FileSorterNew
        {
            public IAction Action { get; set; }

            public List<IFileVisitor> visitors;

            public FileSorterNew()
            {
                visitors = new List<IFileVisitor>();
            }

            public void Sort(string filename)
            {
                FileInfo file = new FileInfo(filename);
                var filetype = visitors.First(v => v.IsMatch(file));
                Action.Do(file, filetype);
            }

        }
    }
}
