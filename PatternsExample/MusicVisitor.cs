﻿using System;
using System.IO;

namespace PatternsExample
{
    class MusicVisitor : IFileVisitor
    {
        public string DestinationFolder => "/Users/francois/Downloads/Music";

        public bool IsMatch(FileInfo filename)
        {
            return filename.Extension == ".mp3";
        }
    }
}
