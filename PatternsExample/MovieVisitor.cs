﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace PatternsExample
{
    class MovieVisitor : IFileVisitor
    {
        readonly List<string> ValidExtentions = new List<string>() { ".avi", ".mkv", ".mpg" };

        public string DestinationFolder => "/Users/francois/Downloads/Movies";

        public bool IsMatch(FileInfo file)
        {
            return ValidExtentions.Contains(file.Extension) &&
                    file.Length > 200000;
        }
    }


    class SeriesVisitor : IFileVisitor
    {
        readonly Regex match = new Regex(".*?(S\\d{2}E\\d{2}).*?");

        public string DestinationFolder => "/Users/francois/Downloads/Series";

        public bool IsMatch(FileInfo file)
        {
            return match.IsMatch(file.Name);
        }
    }

}
