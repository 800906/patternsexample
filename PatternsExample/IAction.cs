﻿using System;
using System.IO;

namespace PatternsExample
{
    interface IAction
    {
        void Do(FileInfo file, IFileVisitor type);
    }

    class CopyAction : IAction
    {
        public void Do(FileInfo file, IFileVisitor type)
        {
            File.Copy(file.FullName, Path.Combine(type.DestinationFolder, file.Name));
        }
    }

    class MoveAction : IAction
    {
        public void Do(FileInfo file, IFileVisitor type)
        {
            File.Move(file.FullName, Path.Combine(type.DestinationFolder, file.Name));
        }
    }
}
