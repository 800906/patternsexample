﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PatternsExample
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSorter sorter = new FileSorter();
            var files = Directory.GetFiles("/Users/francois/Downloads");
            foreach (string file in files)
            {
                sorter.Sort(file);
            }

        }
    }

    class FileSorter
    {
        Regex match = new Regex(".*?(S\\d{2}E\\d{2}).*?");

        public void Sort(string filename)
        {
            FileInfo file = new FileInfo(filename);
            if (file.Length > 200000)
            {
                if (file.Extension != ".mp3" && !match.IsMatch(filename))
                    file.CopyTo("/Users/francois/Downloads/Movies");
                else
                    file.CopyTo("/Users/francois/Downloads/Music");
            }

            if (file.Extension == ".mp3")
            {
                file.CopyTo("/Users/francois/Downloads/Music");
            }

            if (match.IsMatch(file.FullName))
            {
                file.CopyTo("/Users/francois/Downloads/Series");
            }

            if (file.Extension == ".jpg")
            {
                file.CopyTo("/Users/francois/Downloads/Pictures");
            }

            if (file.Extension == ".bmp" && file.Extension == ".png")
            {
                file.CopyTo("/Users/francois/Downloads/Pictures");
            }

            if (file.Extension == ".pdf")
            {
                File.Copy(filename, Path.Combine("/Users/francois/Downloads", filename));
            }

        }
    }





}
